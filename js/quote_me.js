$(function() {


  $("#quote-form input,#quote-form textarea,#quote-form select").jqBootstrapValidation({

    preventSubmit: true,

    submitError: function($form, event, errors) {

      // additional error messages or events

    },

    submitSuccess: function($form, event) {

      event.preventDefault(); // prevent default submit behaviour

      // get values from FORM

      var name = $("input#quotename").val();

      var email = $("input#quoteemail").val();

      var phone = $("input#phone").val();

      var company = $("input#company").val();

      var vessel = $("input#vessel").val();

      var port = $("select#port").val();

      var datepicker = $("input#datepicker").val();

      var requirement = $("textarea#requirement").val();

      var formData = new FormData();

if($('#file')[0].files.length>0){
for(i=0;i<$('#file')[0].files.length;i++){
  formData.append("image[]", $('#file')[0].files[i]);
}

}
        
      //formData.append("image[]", $('#file')[0].files[0]);

formData.append("name", name);

formData.append("phone", phone);

formData.append("email", email);

formData.append("company", company);

formData.append("vessel",vessel);

formData.append("port", port);

formData.append("datepicker", datepicker);

formData.append("requirement", requirement);


      var firstName = name; // For Success/Failure Message

      // Check for white space in name for Success/Fail message

      if (firstName.indexOf(' ') >= 0) {

        firstName = name.split(' ').slice(0, -1).join(' ');

      }

      $this = $("#sendMessageButton1");

      $this.prop("disabled", true); // Disable submit button until AJAX call is complete to prevent duplicate messages

      $.ajax({

        url: "././mail/quote_me.php",

        type: "POST",

        data: formData,

	//{

         // name: name,

         // phone: phone,

        //  company: company,

         // vessel: vessel,

         // port: port,

        //  datepicker: datepicker,

         // email: email,

        //  requirement: requirement,

	//formData:formData,

       // },

        cache: false,

	contentType: false,

        processData: false,

        success: function() {

          // Success message

          var html="<div class='alert alert-success'> <strong>Your message has been sent. </strong>"+

          "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button></div>";

          $('#quoteSuccess').html(html);

          setTimeout(function(){$('#quoteSuccess .alert.alert-success button').click()}, 3000);
          



          //clear all fields

          $('#quote-form').trigger("reset");

        },

        error: function() {

          // Fail message

          var html="<div class='alert alert-danger'>"+

          "Sorry " + firstName + ", it seems that my mail server is not responding. Please try again later!"+

          "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+

          "</div>";

          $('#quoteSuccess').html(html);

          setTimeout(function(){$('#quoteSuccess .alert.alert-danger button').click()}, 3000);

          

          //clear all fields

          $('#quote-form').trigger("reset");

        },

        complete: function() {

          setTimeout(function() {

            $this.prop("disabled", false); // Re-enable submit button when AJAX call is complete

          }, 1000);

        }

      });

    },

    filter: function() {

      return $(this).is(":visible");

    },

  });



  $("a[data-toggle=\"tab\"]").click(function(e) {

    e.preventDefault();

    $(this).tab("show");

  });

});



/*When clicking on Full hide fail/success boxes */

$('#quotename').focus(function() {

  $('#quoteSuccess').html('');

});



